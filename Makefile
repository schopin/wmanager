# Makefile for WManager
#
# Copyright (C) 1999  M. Tessmer
# Copyright (C) 2016, 2020  Peter Pentchev
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



# make settings
SHELL     = /bin/sh
MAKEFLAGS = --print-directory
PREFIX    ?= /usr
X11BASE   ?= /usr

# base settings
bindir	= ${PREFIX}/bin
mandir	= ${PREFIX}/share/man/man
incdir	= ./inc
srcdir	= ./src
fltkinc	= ${X11BASE}/include
x11lib	= ${X11BASE}/lib

FLTK_CONFIG	?= fltk-config --use-gl
FLTK_CXX	:= $(shell ${FLTK_CONFIG} --cxx)
FLTK_CXXFLAGS	:= $(shell ${FLTK_CONFIG} --cxxflags)
FLTK_LDFLAGS	:= $(shell ${FLTK_CONFIG} --ldflags)

# compiler settings
CXX		?= ${FLTK_CXX}
CXXFLAGS_WARN	?= -Wall -W
CXXFLAGS_OPT	?= -O2 -fno-strict-aliasing -pipe
#CXXFLAGS_DBG	?= -g -DDEBUG
CXXFLAGS_DBG	?=
CXXFLAGS	?= ${CXXFLAGS_DBG} ${CXXFLAGS_OPT}
CXXFLAGS	+= ${CXXFLAGS_WARN} -I${incdir} ${FLTK_CXXFLAGS}

# linker settings
LD        = ${CXX}
LDFLAGS   ?= ${FLTK_LDFLAGS}

# install settings
BINOWN		?= root
BINGRP		?= root
BINMODE		?= 755

SHAREOWN	?= ${BINOWN}
SHAREGRP	?= ${BINGRP}
SHAREMODE	?= 644

MKDIR		?= mkdir -p
INSTALL		?= install
STRIP		?= -s
RM		?= rm -f

GZIP		?= gzip
GZIP_N		?= -n
GZIP_C		?= -c

INSTALL_PROGRAM	?= ${INSTALL} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE} ${STRIP}
INSTALL_SCRIPT	?= ${INSTALL} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA	?= ${INSTALL} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}


# search paths
vpath %.H    ${incdir}
vpath %.cc   ${srcdir}
vpath %.1    ${srcdir}

# files
HDR  = Interface.H WManager.H
SRC  = Interface.cc WManager.cc wmanager.cc
MISC = HISTORY README COPYING INSTALL TAGS FAQ LICENSE NEWS TODO Makefile \
	.wmanagerrc .xinitrc
OBJ  = $(patsubst %.cc,%.o,$(SRC)) 
PROG = wmanager
MAN1 = ${srcdir}/${PROG}.1
MAN1Z = ${MAN1}.gz

LOOP	= tools/wmanager-loop
LOOP1	= ${LOOP}.1
LOOP1Z	= ${LOOP1}.gz

UPD	= tools/wmanagerrc-update
UPD1	= ${UPD}.1
UPD1Z	= ${UPD1}.gz

# project version
VERSION = 0.3.0

all:	${PROG} ${MAN1Z} ${LOOP1Z} ${UPD1Z}

${PROG}:	${OBJ}
	${LD} -o $@ $^ ${LDFLAGS} 

${MAN1Z}:	${MAN1}
	${GZIP} ${GZIP_N} ${GZIP_C} ${MAN1} > $@ || (${RM} $@; false)

${LOOP1Z}:	${LOOP1}
	${GZIP} ${GZIP_N} ${GZIP_C} ${LOOP1} > $@ || (${RM} $@; false)

${UPD1Z}:	${UPD1}
	${GZIP} ${GZIP_N} ${GZIP_C} ${UPD1} > $@ || (${RM} $@; false)

test:	all
	[ -n "$$(command -v xvfb-run)" ]
	[ -n "$$(command -v xauth)" ]
	[ -n "$$(command -v xev)" ]
	[ -n "$$(command -v cnee)" ]
	env PATH="$$(pwd):$$PATH" xvfb-run -a -- tests/wmseq

install: all
	${MKDIR} ${DESTDIR}${bindir}
	${INSTALL_PROGRAM} ${PROG} ${DESTDIR}${bindir}/
	${INSTALL_SCRIPT} ${LOOP} ${UPD} ${DESTDIR}${bindir}/
	
	${MKDIR} ${DESTDIR}${mandir}1
	${INSTALL_DATA} ${MAN1Z} ${LOOP1Z} ${UPD1Z} ${DESTDIR}${mandir}1/

clean:
	${RM} ${OBJ} ${PROG} ${MAN1Z} ${LOOP1Z} ${UPD1Z}

version:
	@-echo -e '\nwmanager - X11 window manager selector'
	@-echo 'v${VERSION} by M. Tessmer and Peter Pentchev'
	@-echo 'Look at README for more information\n'


# phony targets
.PHONY: all clean distclean TAGS version
