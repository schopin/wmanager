// wmanager.cc
//
// main file
//
// Copyright (C) 1999  M. Tessmer
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


// SYSTEM INCLUDES //////////////////////////////////
#include <iostream>                               // standard io interface


// PROJECT INCLUDES ////////////////////////////////
#include "WManager.H"                             // main header file


// LOCAL INCLUDES /////////////////////////////////////////////////////////////


int
main(int    argc,
     char** argv)
{
#if DEBUG == 1
  cerr << "wmanager: main(int, char**)\n";
#endif

  WManager* wm = 0;
  int       i  = 0;

  
  wm = new WManager();

  wm->Init();
  i = wm->CheckArguments(argc, argv);
  wm->ParseFile();
  wm->Run(i, argv);

  delete wm;

  return 0;
}
