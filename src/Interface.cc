// Interface.cc
// 
// Implementation of Interface class for WManager
//
// Copyright (C) 1999  M. Tessmer
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


// SYSTEM INCLUDES //////////////////////////////////
#include <iostream>                               // standard io interface
#include <string.h>


// PROJECT INCLUDES ////////////////////////////////
#include "Interface.H"                             // class definition


// LOCAL INCLUDES /////////////////////////////////////////////////////////////
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Pixmap.H>
#include <FL/fl_message.H>

using namespace std;

// logo image
static unsigned char *image_mt[] = {
  (unsigned char*)
  "70 32 2 1",
  (unsigned char*)
  " \tc None",
  (unsigned char*)
  ".\tc #000000",
  (unsigned char*)
  " ...     ...     .....................................................",
  (unsigned char*)
  " ...     ...     .....................................................",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                                  ",
  (unsigned char*)
  " ...     ...     ...                                             .....",
  (unsigned char*)
  " ...     ...     ...                                             .....",
  (unsigned char*)
  " ...     ...     ...                                             .....",
  (unsigned char*)
  " ...     ...     ...                                             .....",
  (unsigned char*)
  " ...     ...     ...                                             .....",
  (unsigned char*)
  "                                                                      "
};
static Fl_Pixmap pixmap_mt(image_mt);



// constructor
Interface::Interface()
{
  _WMWindow    = 0;
  _ExitButton  = 0;
  _StartButton = 0;
  _AboutButton = 0;
  _ListBrowser = 0;
}


// destructor
Interface::~Interface()
{
  delete _WMWindow;
}


// initialize interface
void
Interface::Init() {
  
  // main window
  _WMWindow = new Fl_Window(312, 124, "WManager");
  _WMWindow->user_data((void*)(this));

  // exit button
  _ExitButton = new Fl_Button(210, 100, 100, 20,
			      "E&xit this session");
  _ExitButton->box(FL_THIN_UP_BOX);
  _ExitButton->labelsize(12);
  _ExitButton->callback((Fl_Callback*)_ExitButtonCb, (void*)(this));
   
  // start button
  _StartButton = new Fl_Button(210, 5, 100, 20,
			       "Start");
  _StartButton->shortcut(FL_Enter);
  _StartButton->box(FL_THIN_UP_BOX);
  _StartButton->labelsize(12);
  _StartButton->callback((Fl_Callback*)_StartButtonCb, (void*)(this));
  
  // about button with image
  _AboutButton = new Fl_Button(210, 35, 100, 55);
  _AboutButton->box(FL_ENGRAVED_FRAME);
  pixmap_mt.label(_AboutButton);
  _AboutButton->labelsize(12);
  _AboutButton->callback((Fl_Callback*)_AboutButtonCb, (void*)(this));
  _AboutButton->shortcut('h');
  
  // list with available window managers
  _ListBrowser = new Fl_Hold_Browser(2, 3, 199, 120);
  _ListBrowser->box(FL_THIN_DOWN_BOX);
  _ListBrowser->selection_color(0);
  _ListBrowser->textsize(12);
  _ListBrowser->callback((Fl_Callback*)_ListBrowserCb, (void*)(this));
  _ListBrowser->when(FL_WHEN_NOT_CHANGED);
  _WMWindow->end();
}


// exit button callback
inline void
Interface::_ExitButtonCb_i(Fl_Button*, void*)
{
#if DEBUG == 1
  cerr << "ExitButton()\n";
#endif
  
  cout << "-1\n";

  _WMWindow->hide();
}
void
Interface::_ExitButtonCb(Fl_Button* o, void* v)
{
  ((Interface*)(o->parent()->user_data()))->_ExitButtonCb_i(o,v);
}


// start button
inline void
Interface::_StartButtonCb_i(Fl_Button*, void*)
{
#if DEBUG == 1
  cerr << "StartButton()\n";
#endif
  
  int i = 0;

  i = _ListBrowser->value();

  cout << (char*)(_ListBrowser->data(i))
       << "\n";
  
  _WMWindow->hide();
}
void
Interface::_StartButtonCb(Fl_Button* o, void* v) {
  ((Interface*)(o->parent()->user_data()))->_StartButtonCb_i(o,v);
}


inline void
Interface::_AboutButtonCb_i(Fl_Button*, void*)
{
#if DEBUG == 1
  cerr << "AboutButton()\n";
#endif

  fl_message("WManager v0.2\n"
	     "(c) 1999 by M . Tessmer <mtessmer@techfak.uni-bielefeld.de>\n"
	     "(c) 2016 by Peter Pentchev <roam@ringlet.net>\n");
}
void
Interface::_AboutButtonCb(Fl_Button* o, void* v)
{
  ((Interface*)(o->parent()->user_data()))->_AboutButtonCb_i(o,v);
}


//: List browser callback function
inline void
Interface::_ListBrowserCb_i(Fl_Browser*, void*)
{
#if DEBUG == 1
  cerr << "ListBrowser()\n";
#endif

  int i = 0;
  
  i = _ListBrowser->value();
  if(_ListBrowser->visible(i) == 0) {
    _ListBrowser->show(i);
  }

#if DEBUG == 1
  cerr << "Selected: "
       << _ListBrowser->text(i) << " ("
       << (char*)(_ListBrowser->data(i))
       << ")\n";
#endif
}
void
Interface::_ListBrowserCb(Fl_Browser* o, void* v)
{
  ((Interface*)(o->parent()->user_data()))->_ListBrowserCb_i(o,v);
}


// add window manager to list
void
Interface::AddToBrowser(const char* name,
			const char* path)
{
#if DEBUG == 1
  cerr << "Interface::AddToBrowser(const char*, const char*)\n";

  cerr << "Interface::AddToBrowser: name = <" << name
       << ">, path = <" << path
       << ">\n";
#endif

  char* path_buffer = strdup(path);

  _ListBrowser->add(name, (void*)path_buffer);
} 


// enter fltk run loop
void
Interface::Run(int    argc,
	       char** argv)
{
#if DEBUG == 1
  cerr << "Run()\n";
#endif

  _ListBrowser->select(1);
  _WMWindow->show(argc, argv);

  Fl::run();
}


// test routine
//  int
//  main(int    argc,
//       char** argv)
//  {
//    Interface* interface = new Interface();
//    interface->Init();

//    interface->AddToBrowser("test windowmanager", "test_path");
//    interface->Run(argc, argv);

//  }
